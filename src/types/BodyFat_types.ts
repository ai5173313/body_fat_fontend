export default interface Item {
  Weight : number;
  Chest : number;
  Abdomen : number;
  Hip : number;
  Thigh : number;
}
