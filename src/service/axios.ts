import axios from "axios";

const instance = axios.create({
  baseURL: 'http://localhost:8000',

});
const http = instance

function getItem(item : any) {
  return http.put("/items", item)
}
export default { getItem }